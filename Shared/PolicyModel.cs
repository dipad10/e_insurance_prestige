﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace E_insurance_prestige.Shared
{
    public class PolicyModel
    {
        public string Apikey { get; set; }

        public string PolicyNo { get; set; }

        public string BizOption { get; set; }
        public DateTime? BillingDate { get; set; }
        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public decimal? SumInsured { get; set; }

        public decimal? GrossPremium { get; set; }

        public string BrokerPortalId { get; set; }

        public InsuredClientModel insuredClient { get; set; } = new InsuredClientModel();


        public VehicledetailsModel vehicledetails { get; set; } = new VehicledetailsModel();
    }
}

