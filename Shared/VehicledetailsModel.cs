﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace E_insurance_prestige.Shared
{
    public class VehicledetailsModel
    {
        public string Vehiclemake { get; set; }
        public string Vehiclemodel { get; set; }
        public string Vehicleyear { get; set; }
        public string colour { get; set; }
        public string Chasisnumber { get; set; }
        public string Enginenumber { get; set; }
        public string Registrationnumber { get; set; }
        public DateTime? Dateofregistration { get; set; }
        private decimal? _SumInsured;
        public decimal? SumInsured
        {
            get
            {
                return _SumInsured;
            }
            set
            {
                _SumInsured = value;
            }
        }
        private decimal? _Premiumamount;
        public decimal? Premiumamount
        {
            get
            {
                return _Premiumamount;
            }
            set
            {
                _Premiumamount = value;
            }
        }
    }
}

