﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace E_insurance_prestige.Shared
{
    public class InsuredClientModel
    {
        public string Surname { get; set; }
        public string FirstName { get; set; }
        public string OtherNames { get; set; }
        public string Occupation { get; set; }
        public string Address { get; set; }

        public string MobilePhone { get; set; }
        public string LandPhone { get; set; }

        public string Email { get; set; }
        public string Fax { get; set; }

        public string Remarks { get; set; }
    }
}

