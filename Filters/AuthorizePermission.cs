﻿using E_insurance_prestige.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace E_insurance_prestige.Filters
{
    public class AuthorizePermission : System.Web.Mvc.ActionFilterAttribute, System.Web.Mvc.IActionFilter
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var userpermission = new validationhelper().GetUserPermission(HttpContext.Current.Session["currentuser"].ToString());
            if (!userpermission.Equals("ADMIN"))
            {
                var url = filterContext.HttpContext.Request.Url.ToString();
                var routeDictionary = new RouteValueDictionary { { "Action", "Login" }, { "Controller", "Account" }, { "returnurl", url } };
                filterContext.Result = new RedirectToRouteResult(routeDictionary);
            }
            base.OnActionExecuting(filterContext);
        }
    }
}