﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using E_insurance_prestige.Modules;

namespace E_insurance_prestige.Filters
{
    public class LogActions : ActionFilterAttribute, IActionFilter
    {
        void IActionFilter.OnActionExecuting(ActionExecutingContext filterContext)
        {
            // TODO: Add your action filter's tasks here

            // Log Action Filter call
            
            var Controller = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            var Action = string.Concat(filterContext.ActionDescriptor.ActionName, " (Logged By: App Action Filter)");
            var IP = filterContext.HttpContext.Request.UserHostAddress;
            var DateTime = filterContext.HttpContext.Timestamp;
            var method = filterContext.HttpContext.Request.HttpMethod;
            var url = filterContext.HttpContext.Request.Url.ToString();
            string message = $"{IP} | {url} | {method}";
            HelperStoreSqlLog.LogInformation(null, message);   
                OnActionExecuting(filterContext);
            
        }
    }
}