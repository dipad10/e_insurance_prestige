﻿using E_insurance_prestige.Entity;
using E_insurance_prestige.Services;
using E_insurance_prestige.Shared;
using E_insurance_prestige.Viewmodels.Home;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using RestSharp;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Net;

namespace E_insurance_prestige.Modules

{
    public class ModDrive
    {
        GibsBrokersDBEntities db = new GibsBrokersDBEntities();

        #region RESPONSES
        public enum ErrCodeEnum
        {
            NO_ERROR = 0,
            INVALID_GUID = 200,
            INVALID_PASSWORD = 200,
            FORBIDDEN = 407,
            INVALID_RANGE = 408,
            ACCOUNT_LOCKED = 400,
            ACCOUNT_EXPIRED = 400,
            GENERIC_ERROR = 100
        }

        public class Response
        {
            public int ErrorCode;
            public string ErrorMessage;
            public string ExtraMessage;
            public int TotalSuccess;
            public int TotalFailure;
     
        }

        public static Response _GetResponseStruct(ErrCodeEnum ErrCode, int TotalSuccess = 0, int TotalFailure = 0, string ErrorMsg = "", string ExtraMsg = "")
        {
            Response res = new Response
                {
                ErrorCode = Convert.ToInt32(ErrCode),
                ErrorMessage = ErrorMsg,
                ExtraMessage = ExtraMsg,
                TotalSuccess = TotalSuccess,
                TotalFailure = TotalFailure

            };
   
            return res;
        }
        #endregion
        #region DATA ENCRYPTION

        public string Encrypt(string clearText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6E, 0x20, 0x4D, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }


        public string Decrypt(string cipherText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6E, 0x20, 0x4D, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }


        #endregion

        #region GET FUNCTIONS
        public List<int> Getlistofyears()
        {

            return Enumerable.Range(1995, DateTime.Now.Year - 1994).OrderByDescending(i => i).ToList();
        }

        public string FormatNumber(int num)
        {
            if (num >= 100000)
                return FormatNumber(num / 1000) + "K";
            if (num >= 10000)
            {
                return (num / 1000D).ToString("0.#") + "K";
            }
            return num.ToString("#,0");
        }

        public string Getcurrentuser()
        {
            return HttpContext.Current.Session["userid"].ToString();
        }
        public string Getcurrentuserfirstname()
        {
            return HttpContext.Current.Session["firstname"].ToString();
        }
        #endregion

        public string PopulateBodyproductsuccessful(string firstname, string producttype)
        {
            string body = string.Empty;
            StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Templates/EmailTemplateProductsuccessful.html"));
            body = reader.ReadToEnd();
            body = body.Replace("{firstname}", firstname);
            body = body.Replace("{producttype}", producttype);
          
            return body;
        }

        public string PopulateBodyForgotPassword(string Fullname, string Url)
        {
            string body = string.Empty;
            StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Templates/EmailTemplateforgotpwd.html"));
             body = reader.ReadToEnd();
            body = body.Replace("{Username}", Fullname);
            body = body.Replace("{Url}", Url);

            return body;
        }




        public string SendHtmlFormattedEmail(string recepientEmail, string cc, string subject, string body)
        {
            try
            {
                System.Net.Mail.MailMessage mailMessage = new System.Net.Mail.MailMessage();
                mailMessage.From = new MailAddress(ConfigurationManager.AppSettings["UserName"], "PPrestige Insurance ");
                mailMessage.Subject = subject;
                mailMessage.Body = body;
                mailMessage.IsBodyHtml = true;
                mailMessage.To.Add(new MailAddress(recepientEmail));
                if (cc != "")
                {
                    string[] CCId = cc.Split(',');

                    foreach (string CCEmail in CCId)
                        // Adding Multiple CC email Id
                        mailMessage.CC.Add(new MailAddress(CCEmail));
                }
                else
                {
                }

                SmtpClient smtp = new SmtpClient();
                smtp.Host = ConfigurationManager.AppSettings["Host"];
                smtp.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]);
                System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
                NetworkCred.UserName = ConfigurationManager.AppSettings["UserName"];
                NetworkCred.Password = ConfigurationManager.AppSettings["Password"];
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = int.Parse(ConfigurationManager.AppSettings["Port"]);
                smtp.Send(mailMessage);
                HelperStoreSqlLog.LogInformation(null, $"Successfully sent email to {recepientEmail}");

                return "success";
            }
            catch (Exception ex)
            {
                HelperStoreSqlLog.LogError(ex, $"Failed to send email to {recepientEmail}");

                return "fail";
            }
        }

        public string DoVehicleDetailsUploadNIID(int? certno)
        {
            var service = new PolicyService();
            var vm = service.GetPolicyByCertno2(certno);
            string PolicyNo = vm.PolicyNo;

            string InsuredName = vm.Insured_name;
            string contactAddress = vm.Address;
            string GSMSNo = vm.Phone;
            string EmailAddress = vm.Email;
                        string TypeofCover = "T";
            string VehCategory = vm.Category;
            string EngineNo = vm.Engine_number;
            string ChassisNo = vm.Chasis_number;
            string VehColor = vm.Colour;

            string YearOfMake = vm.Vehicle_year;

             string RegNo = vm.Registeration_number;

              string OldRegNo = RegNo;

            string StartDate = vm.Effective_date.ToString();
            string EndDate = vm.Expiry_date.ToString(); ;
            string VehType = vm.Vehicle_model;
                        string EngineCapacity = "nil";

            string Field23 = vm.Vehicle_make;
                        string Field24 = VehType;
                        string SumInsured = "0";
                        string NetAmount = "0";
                        string CoverNoteNum = "0";


            string CertificateNum = certno.ToString();


                        string GeographicalZone = VehType;

                        niid.ServiceSoap.Service SDK = new niid.ServiceSoap.Service();
                        string msg = "";
            HelperStoreSqlLog.LogInformation(null, $"Doing NIID Upload for Certificate No {certno}");
            try
            {
                msg = SDK.Vehicle_Policy_Push("ogbanje", "Emma1234", "", PolicyNo, InsuredName, contactAddress, GSMSNo, EmailAddress, StartDate, EndDate, TypeofCover, VehCategory, EngineNo, ChassisNo, VehColor, YearOfMake, Field23 + " " + Field24, RegNo, OldRegNo, VehType, EngineCapacity, Field24, 0, 0, CoverNoteNum, CertificateNum, GeographicalZone);
                if (msg.ToLower() == "successful")
                {
                    HelperStoreSqlLog.LogInformation(null, $"NIID Upload successful for Certificate No {certno}");
                    return msg;
                }
                else { return msg; }
            }
            catch (Exception ex)
            {
                HelperStoreSqlLog.LogError(ex, $"NIID Upload Fail for Certificate No {certno}");
            }
            return msg;
                       
                            
                    
        }

        public string GetNextValue()
        {
            var rec = db.InsCompanies.Where(o => o.CompanyName.Equals("PRESTIGE")).SingleOrDefault();
            return rec.NextValue.ToString();
        }

        public void UpdateAuto()
        {
            var rec = db.InsCompanies.Where(o => o.CompanyName.Equals("PRESTIGE")).SingleOrDefault();
            rec.NextValue = rec.NextValue + 1;
            db.SaveChanges();
        }

        public string GeneratePolicynumber()
        {
            var nextvalue = GetNextValue();
            string format = $"PONL/{DateTime.Now.Year.ToString("yy")}/{DateTime.Now.Month.ToString("MM")}/P/0000{nextvalue}";

            return format;
        }

        public string Serializedata(int? certno)
        {
            PolicyService polservice = new PolicyService();
            UsersService b = new UsersService();
            ERegisteration rec = polservice.GetPolicyByCertno2(certno);

            PolicyModel model = new PolicyModel();          
                model.Apikey = ConfigurationManager.AppSettings["dipadapikey"];
                model.BillingDate = rec.Effective_date;
                model.BizOption = rec.field02;
                model.BrokerPortalId = rec.Certificate_no.ToString();
                model.EndDate = rec.Expiry_date;
                model.GrossPremium = rec.Amount;
                model.PolicyNo = rec.PolicyNo;
                model.StartDate = rec.Effective_date;
                model.SumInsured = rec.Amount;
                model.vehicledetails.Premiumamount = rec.Amount;
                model.vehicledetails.Registrationnumber = rec.Registeration_number;
                model.vehicledetails.SumInsured = rec.Amount;
                model.vehicledetails.Vehiclemake = rec.Vehicle_model;
                model.vehicledetails.Vehiclemodel = rec.Vehicle_model;
                model.vehicledetails.Vehicleyear = rec.Vehicle_year;
                model.vehicledetails.Chasisnumber = rec.Chasis_number;
                model.vehicledetails.colour = rec.Colour;
                model.vehicledetails.Dateofregistration = rec.Effective_date;
                model.vehicledetails.Enginenumber = rec.Engine_number;
                model.insuredClient.Address = rec.Address;
                model.insuredClient.Email = rec.Email;
                model.insuredClient.Fax = rec.Phone;
                model.insuredClient.FirstName = rec.Insured_name;
                model.insuredClient.LandPhone = rec.Phone;
                model.insuredClient.MobilePhone = rec.Phone;
                model.insuredClient.Occupation = rec.Occupation;
                model.insuredClient.OtherNames = rec.Insured_name;
                model.insuredClient.Remarks = System.Guid.NewGuid().ToString();
                model.insuredClient.Surname = rec.Insured_name;
            

            string data = JsonConvert.SerializeObject(model);

            return data;
        }

        public int PostPBS(int? certno)
        {
            //call serialiaze data funtion to do its job
            HelperStoreSqlLog.LogInformation(null, $"Pushing to PBS for Certificate No {certno}");
            string postdata = Serializedata(certno);
            
            try
            {
                string url = $"{ConfigurationManager.AppSettings["pathtoapitst"]}/policy/postpolicy";
                var client = new RestClient(url);
                var request = new RestRequest(postdata, DataFormat.Json);
               var response = client.Post(request);
                HttpStatusCode code = response.StatusCode;
                if(code.Equals(200))
                {
                    HelperStoreSqlLog.LogInformation(null, $"Push to PBS successful for Certificate No {certno}");

                    return (int)response.StatusCode;
                }
                else {
                    HelperStoreSqlLog.LogError(null, $"Push to PBS failed for Certificate No {certno}. Returned a {response.StatusCode} with body {response.Content}");
                    return (int)response.StatusCode;
                }
            }
            catch (Exception ex)
            {
                HelperStoreSqlLog.LogError(ex, $"Push to PBS failed for Certificate No {certno}");
            
            }
            return 0;
            
        }

    }
}