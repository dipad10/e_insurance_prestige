﻿//using IPolicy.Data.Repository;
using E_insurance_prestige.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace E_insurance_prestige.Modules
{
    public class Filldropdownhelper
    {
        GibsBrokersDBEntities db = new GibsBrokersDBEntities();
        ModDrive mod = new ModDrive();
        public SelectList FillStates()
        {
          
            var States = new SelectList(GetStates(), "Region", "Region");
            return States;

        }
        public SelectList FillBranches()
        {
            var Branches = new SelectList(GetBranches(), "Description", "Description");
            return Branches;
        }
        public SelectList FillYears()
        {

            var Years = new SelectList(mod.Getlistofyears());
            return Years;

        }

        public SelectList FillVehicleMakes()
        {

            var Makes = new SelectList(GetVehicleMakes(), "make", "make");
            return Makes;

        }


        public List<SelectListItem> Fillusersfilter()
        {
            List<SelectListItem> lstitem = new List<SelectListItem>();
            lstitem.Add(new SelectListItem { Text = "Username", Value = "Username" });
            lstitem.Add(new SelectListItem { Text = "Name", Value = "Name" });
            return lstitem;
        }
        public List<SelectListItem> FillUsage()
        {
            List<SelectListItem> lstitem = new List<SelectListItem>();
            lstitem.Add(new SelectListItem { Text = "Private", Value = "Private" });
            lstitem.Add(new SelectListItem { Text = "Commercial", Value = "Commercial" });
            return lstitem;
        }
        public List<SelectListItem> FillvehicleCategory()
        {
            List<SelectListItem> lstitem = new List<SelectListItem>();
            lstitem.Add(new SelectListItem { Text = "Private Car", Value = "5000" });
            lstitem.Add(new SelectListItem { Text = "Commercial Vehicle", Value = "7500" });
            lstitem.Add(new SelectListItem { Text = "Tricycle", Value = "2500" });
            lstitem.Add(new SelectListItem { Text = "MotorCycle", Value = "1500" });
            return lstitem;
        }

    
        public List<SelectListItem> FillInsuredtypes()
        {
            List<SelectListItem> lstitem = new List<SelectListItem>();
            lstitem.Add(new SelectListItem { Text = "Individual", Value = "B" });
            lstitem.Add(new SelectListItem { Text = "Corporate", Value = "A" });
            return lstitem;

        }
        public List<SelectListItem> FillIdentification()
        {
            List<SelectListItem> lstitem = new List<SelectListItem>();
            lstitem.Add(new SelectListItem { Text = "National ID", Value = "National ID" });
            lstitem.Add(new SelectListItem { Text = "Driver's Licence", Value = "Driver's Licence" });
            lstitem.Add(new SelectListItem { Text = "International Passport", Value = "International Passport" });
            lstitem.Add(new SelectListItem { Text = "Permanent Voters Card", Value = "Permanent Voters Card" });
            return lstitem;

        }

        public List<SelectListItem> Fillusertypes()
        {
            List<SelectListItem> lstitem = new List<SelectListItem>();
            lstitem.Add(new SelectListItem { Text = "Normal", Value = "O" });
            lstitem.Add(new SelectListItem { Text = "Super", Value = "A" });
            return lstitem;

        }

        public List<SelectListItem> Filluserdepartments()
        {
            List<SelectListItem> lstitem = new List<SelectListItem>();
            lstitem.Add(new SelectListItem { Text = "Department 1", Value = "T" });
            lstitem.Add(new SelectListItem { Text = "Department 2", Value = "B" });
            return lstitem;

        }

        public List<SelectListItem> FilluserGroups()
        {
            List<SelectListItem> lstitem = new List<SelectListItem>();
            lstitem.Add(new SelectListItem { Text = "Group A", Value = "1" });
            lstitem.Add(new SelectListItem { Text = "Group B", Value = "2" });
            lstitem.Add(new SelectListItem { Text = "Group C", Value = "3" });
            lstitem.Add(new SelectListItem { Text = "Group D", Value = "4" });
            lstitem.Add(new SelectListItem { Text = "Group E", Value = "5" });
            lstitem.Add(new SelectListItem { Text = "Group F", Value = "6" });
            lstitem.Add(new SelectListItem { Text = "Group G", Value = "7" });
            lstitem.Add(new SelectListItem { Text = "Group H", Value = "8" });
            lstitem.Add(new SelectListItem { Text = "Group I", Value = "9" });

            return lstitem;

        }

        #region GET STATIC DATA

        public List<State> GetStates()
        {
            return db.States.ToList();
        }

        public List<VehMake> GetVehicleMakes()
        {
            return db.VehMakes.ToList();
        }

        public List<Branch> GetBranches()
        {
            return db.Branches.ToList();
        }
        public List<string> GetVehicleMakebrandById(string make)
        {
            return db.VehMakeBrands.Where(p => p.Make == make).Select(o => o.FullName).ToList();
        }



        #endregion
    }
}