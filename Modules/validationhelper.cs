﻿using E_insurance_prestige.Entity;
using E_insurance_prestige.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace E_insurance_prestige.Modules
{
    public class validationhelper
    {
        GibsBrokersDBEntities db = new GibsBrokersDBEntities();
        public User Validateuser(string username, string password)
        {
            return db.Users.Where(p => p.Username == username && p.Password == password).FirstOrDefault();
        }

        public string GetUserPermission(string username)
        {
            return db.Users.Where(p => p.Username == username)
                .Select(o => o.Permissions)
                .FirstOrDefault();
        }

        public Boolean CheckPinExists(string pin)
        {

            var exists = new ScratchcardService().GetScratchCardByPin(pin);
            if (exists == null)
            {
                return false;
            }
            else
            {
                return true;
            }


        }

        public string CheckPinStatus(string pin)
        {
            var scratchcard = new ScratchcardService().GetScratchCardByPin(pin);
            return scratchcard.Status;
        }

        public Boolean CheckEmailAddressExist(string emailaddress)
        {
            var email = db.Users.Where(o => o.Email.Equals(emailaddress)).FirstOrDefault();
            if(email == null)
            {
                return false;
            }
            else { return true; }
        }

        public Boolean CheckResetlinkExist(string emailaddress, string activationcode)
        {
            var user = db.Users.Where(o => o.Email.Equals(emailaddress) && o.ActivationCode.Equals(activationcode)).FirstOrDefault();
            if (user == null)
            {
                return false;
            }
            else { return true; }
        }
    }

}