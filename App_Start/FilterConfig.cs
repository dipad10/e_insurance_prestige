﻿using System.Web;
using System.Web.Mvc;

namespace E_insurance_prestige
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
