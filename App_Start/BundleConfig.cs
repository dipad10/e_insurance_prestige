﻿using System.Web;
using System.Web.Optimization;

namespace E_insurance_prestige
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/metro").Include(
                       "~/Scripts/metro.js",
                       "~/Scripts/toastr.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                     "~/Content/Site.css",
                     "~/Content/toastr.css",
                      "~/Content/hover-min.css",
                     "~/Content/metro-all.css",
                      "~/Content/metro-animations.css",
                        "~/Content/metro-rtl.css"
                      //"~/Content/metro-colors.css",
                      //"~/Content/metro-icons.css",
                      //"~/Content/metro-rtl.css",
                      // "~/Content/datatables.css",
                      //  "~/Content/select2.css",
                      // "~/Content/metro-colors.css"
                      ));
        }
    }
}
