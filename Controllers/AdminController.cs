﻿using E_insurance_prestige.Filters;
using E_insurance_prestige.Modules;
using E_insurance_prestige.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using E_insurance_prestige.Entity;
using E_insurance_prestige.Filters;
using E_insurance_prestige.Modules;
using E_insurance_prestige.Services;
using E_insurance_prestige.Viewmodels.Home;
using E_insurance_prestige.Viewmodels.Admin;

namespace E_insurance_prestige.Controllers
{
    [Authorizesession]
    [LogActions]
    [AuthorizePermission]
    public class AdminController : Controller
    {
   
        public ActionResult Dashboard()
        {
            var service = new PolicyService();
            ViewBag.TotalPolicycount = service.GetTotalPolicyCount();
            ViewBag.TotalSumInsured = service.GetTotalSumInsured();
            ViewBag.Totaldueforrenewal = service.GetTotalDueForRenewal();
            return View();
        }

        public ActionResult All_Transactions()
        {
            var policies = new PolicyService().GetAllPolicies();
            return View(policies);
        }

        public ActionResult Scratchcards()
        {
            var cards = new ScratchcardService().GetAllScratchCards();
            return View(cards);
        }

        public ActionResult Enable_Disable_Cards(string type, int? serial)
        {
            //update card set status to blocked or unused
            var service = new ScratchcardService();
            service.Enable_DisableCard(type, serial);
            TempData["success"] = true;
            TempData["message"] = $"Scratchcard {serial} {type}d successfully!";
            return RedirectToAction("Scratchcards");
        }

        public ActionResult Branches()
        {
            var branches = new BranchesService().GetAllBranches();
            return View(branches);
        }

        public ActionResult Branches_Edit(int? id)
        {
           //get branch by id and return edit page
            return View();
        }
        public ActionResult Branches_Delete(int? id)
        {
            //delete and redirect back to page and trigger toast
            var service = new BranchesService();
            service.DeleteBranch(id);
            TempData["success"] = true;
            TempData["message"] = $"Branch deleted successfully!";
            return RedirectToAction("Branches");
        }

        public ActionResult Allocate_Cards()
        {
            var service = new ScratchcardService();
            var vm = new Allocate_cardsVM();
            vm.cards = service.GetAllScratchCards();
            ViewBag.branch = new Filldropdownhelper().FillBranches();
            return View(vm);

        }
        [HttpPost]
        public ActionResult Allocate_Cards(Allocate_cardsVM vm)
        {
            var service = new ScratchcardService();
            if (ModelState.IsValid)
            {
                var serialnumbers = vm.SerialNumbers.Split(',').ToList();
                foreach (var serial in serialnumbers)
                {
                    service.UpdateScratchcardBranch(serial, vm.Branch);
                }
                TempData["success"] = true;
                TempData["message"] = $"Cards {vm.SerialNumbers} Allocated to {vm.Branch} successfully!";
                return RedirectToAction("Allocate_Cards");
            }
            return View(vm);

        }

        public ActionResult Transaction_Edit(int? certno)
        {
            var policy = new PolicyService().GetPolicyByCertno2(certno);
           
            return View(policy);

        }
        [HttpPost]
        public ActionResult Transaction_Edit(ERegisteration policy)
        {
            var pol = new PolicyService().UpdatePolicy(policy);

            TempData["success"] = true;
            TempData["message"] = $"Policy Updated successfully!";
            return RedirectToAction("All_Transactions");

        }

        public ActionResult NIID_Upload(int? certno)
        {
            var mod = new ModDrive();
           string response = mod.DoVehicleDetailsUploadNIID(certno);
            if(response.Equals("successful"))
            {
                TempData["success"] = true;
                TempData["message"] = $"NIID Upload Succesfull for {certno}";
                return RedirectToAction("All_Transactions");
            }
            else
            {
                TempData["error"] = true;
                TempData["message"] = $"NIID Upload failed with response {response}";
                return RedirectToAction("All_Transactions");
            }
          
        }

        public ActionResult Push_PBS(int? certno)
        {
            //upload pbs 
            var mod = new ModDrive();
           int code = mod.PostPBS(certno);
            if (code.Equals(200))
            {
                TempData["success"] = true;
                TempData["message"] = $"Data pushed to PBS Succesfully for {certno}";
                return RedirectToAction("All_Transactions");
            }
            else
            {
                TempData["error"] = true;
                TempData["message"] = $"An Error Occured while pushing to PBS for {certno}. Check Applogs for error message";
                return RedirectToAction("All_Transactions");
            }
        }

        public ActionResult Marketers()
        {
            return View();
        }

        public ActionResult Users()
        {
            return View();
        }
    }
}