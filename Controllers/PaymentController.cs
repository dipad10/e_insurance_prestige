﻿using E_insurance_prestige.Viewmodels.Home;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using E_insurance_prestige.Modules;
using E_insurance_prestige.Services;
using E_insurance_prestige.Filters;
using E_insurance_prestige.Entity;

namespace E_insurance_prestige.Controllers
{
    [Authorizesession]
    public class PaymentController : Controller
    {
        // GET: Payment
        public ActionResult Pay()
        {
            PaymentVM vm = new PaymentVM();
            int retries = 0;
            Session["retries"] = Convert.ToString(retries);
            ViewBag.message = $"Please attempt the PIN NOT more than 3 times to avoid being blocked - {Session["retries"]} attempts";
            ViewBag.messagetype = "info";
            var type = Request.QueryString["type"];
            if (type.Equals("R")) { ViewBag.header = $"Make Payment for Policy {Session["PolicyNo"]} Renewal"; }
            return View(vm);
        }

        [HttpPost]
        public ActionResult Pay(PaymentVM vm)
        {
           
            if (ModelState.IsValid)
            {
                //Firsot check if user has exjhausted retries then redirect
                int retries = Convert.ToInt32(Session["retries"]);
                if (Session["retries"].Equals("3"))
                {
                   return Redirect("http://prestigeassuranceplc.com");
                }
                else
                {
                    //Try get scratchcard by Pin
                    var validation = new validationhelper();
                    var scratchcardservice = new ScratchcardService();
                    if (validation.CheckPinExists(vm.Pin))
                    {
                        //check pin status
                        var policyservice = new PolicyService();
                        var moddrive = new ModDrive();
                        ERegisteration pol = new ERegisteration();
                        var status = validation.CheckPinStatus(vm.Pin);
                       
                        switch (status)
                        {
                            case "used":
                                retries = retries + 1;
                                Session["retries"] = Convert.ToString(retries);
                                ViewBag.message = $"Pin has already been Used! No of Retry {retries}";
                                ViewBag.messagetype = "error";
                                return View(vm);                               

                            case "blocked":
                                retries = retries + 1;
                                Session["retries"] = Convert.ToString(retries);
                                ViewBag.message = $"Pin has been Blocked! No of Retry {retries}";
                                ViewBag.messagetype = "error";
                                return View(vm);

                            default:
                                //unused
                               scratchcardservice.UpdateScratchcardStatusByPin(vm.Pin, Session["insuredname"].ToString());
                                //map userdata to createpolicyvm.
                                CreatePolicyVM policyvm = new PolicyService().MapSessionToVM();                          
                                var bizoption = Request.QueryString["type"];
                              
                                if (bizoption.Equals("N") || bizoption == null)
                                {
                                    //new policy so generate policy number
                                   pol = policyservice.MapPolicy(policyvm, vm, "N");
                                    policyservice.AddPolicy(pol);
                                    //send email on about new policy
                                    //string url = $"{ConfigurationManager.AppSettings["pathtoresetpwdtst"]}?M={vm.EmailAddress}&C={user.ActivationCode}";
                                    //string body = moddrive.PopulateBodyForgotPassword(user.Insured_name, url);
                                    //moddrive.SendHtmlFormattedEmail(pol.Email, null, "Thank You", body);
                                }
                                //get policy from session
                                else { policyvm.PolicyNo = Session["PolicyNo"].ToString();
                                    pol = policyservice.MapPolicy(policyvm, vm, "R");
                                    policyservice.AddPolicy(pol);
                                    //send email abt renewal
                                }                                                                                           
                                break;
                        }
                        //continue to other processes niid upload, printing report sms, upload pbs email
                        moddrive.DoVehicleDetailsUploadNIID(pol.Certificate_no);

                    }
                    else
                    {
                        retries = retries + 1;
                        Session["retries"] = Convert.ToString(retries);
                        ViewBag.message = $"Pin is Invalid Please try again. No of Retry {retries}";
                        ViewBag.messagetype = "error";
                        return View(vm);
                    }
                    
                }
            }
            else
            {
               
                return View(vm);
            }

            return View();
        }
            
    }
}
