﻿using E_insurance_prestige.Entity;
using E_insurance_prestige.Filters;
using E_insurance_prestige.Modules;
using E_insurance_prestige.Services;
using E_insurance_prestige.Viewmodels.Home;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace E_insurance_prestige.Controllers
{
    [LogActions]
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult CreatePolicy()
        {
            ViewBag.vehiclemakes = new Filldropdownhelper().FillVehicleMakes();
            ViewBag.states = new Filldropdownhelper().FillStates();
            ViewBag.Years = new Filldropdownhelper().FillYears();
            ViewBag.category = new SelectList(new Filldropdownhelper().FillvehicleCategory(), "Value", "Text");
            ViewBag.Identification = new SelectList(new Filldropdownhelper().FillIdentification(), "Value", "Text");
            ViewBag.Usage = new SelectList(new Filldropdownhelper().FillUsage(), "Value", "Text");
            CreatePolicyVM pol = new CreatePolicyVM();
            pol.Insurance_class = "Third Party Only";
            return View(pol);
        }
        [HttpPost]
        public ActionResult CreatePolicy(CreatePolicyVM vm)
        {
            //pass vm to sessions here and redirect to pay
            Session["Username"] = vm.Username;
            Session["Address"] = vm.Address;
            Session["Title"] = vm.Title;
            Session["Password"] = vm.Password;
            Session["Insuredname"] = vm.Insured_name;
            Session["location"] = vm.location;
            Session["Identification"] = vm.Identification;
            Session["Identification_no"] = vm.Id_number;
            Session["Insurance_class"] = "Third Party Only";
            Session["Email"] = vm.Email;
            Session["Phone"] = vm.Phone;
            Session["Password"] = vm.Password;
            Session["Vehicle_make"] = vm.Vehicle_make;
            Session["Vehicle_model"] = vm.Vehicle_model;
            Session["Vehicle_year"] = vm.Vehicle_year;
            Session["Registration_number"] = vm.Registeration_number;
            
            Session["Engine_number"] = vm.Engine_number;
            Session["Category"] = vm.categorytext;
            Session["Usage"] = vm.Usage;
            Session["Amount"] = vm.Category;
            Session["Occupation"] = vm.Occupation;
            Session["Colour"] = vm.Colour;
          
            string chasis = vm.Chasis_number;
            chasis = chasis.Replace("O", "0");
            chasis = chasis.Replace("I", "1");
            Session["Chasis_number"] = chasis;
            return Redirect("/Payment/pay?type=N");
        }
        [Authorizesession]
        public ActionResult Dashboard()
        {
            var service = new PolicyService();
            var data = service.GetPoliciesByUser(Session["currentuser"].ToString());
            ViewBag.TotalPolicycount = service.GetUserTotalPolicyCount(Session["currentuser"].ToString());
            ViewBag.TotalSumInsured = service.GetUserTotalSumInsured(Session["currentuser"].ToString());
            ViewBag.Totaldueforrenewal = service.GetUserTotalDueForRenewal(Session["currentuser"].ToString());
            return View(data);
        }
    
        [HttpPost]
        public ActionResult PrintCertificate(PrintCertificateVM vm)
        {
            var service = new PolicyService();
            service.GetPolicybyRegNo(vm.VehicleRegNo);
            return View("/report");
        }
        [Authorizesession]
        public ActionResult Policies()
        {
            var service = new PolicyService();
            var data = service.GetPoliciesByUser(Session["currentuser"].ToString());
            return View(data);
        }

        public ActionResult PolicyDetails(int certno)
        {
            var service = new PolicyService();
            var data = service.GetPolicyByCertno(certno);
            ViewBag.policyno = data.PolicyNo;
            return View(data);
        }
        
        public ActionResult PolicyRenewal(int certno)
        {
            var service = new PolicyService();
            var vm = service.GetPolicyByCertno(certno);
            Session["Username"] = vm.Username;
            Session["Address"] = vm.Address;
            Session["Title"] = vm.Title;
            Session["Password"] = vm.Password;
            Session["Insuredname"] = vm.Insured_name;
            Session["location"] = vm.location;
            Session["Identification"] = vm.Identification;
            Session["Identification_no"] = vm.Id_number;
            Session["Insurance_class"] = "Third Party Only";
            Session["Email"] = vm.Email;
            Session["Phone"] = vm.Phone;
            Session["Password"] = vm.Password;
            Session["Vehicle_make"] = vm.Vehicle_make;
            Session["Vehicle_model"] = vm.Vehicle_model;
            Session["Vehicle_year"] = vm.Vehicle_year;
            Session["Registration_number"] = vm.Registeration_number;
            Session["PolicyNo"] = vm.PolicyNo;
            Session["Engine_number"] = vm.Engine_number;
            Session["Category"] = vm.Category;
            Session["Usage"] = vm.Usage;
            Session["Amount"] = vm.Amount;
            Session["Occupation"] = vm.Occupation;
            Session["Colour"] = vm.Colour;
            Session["currentuser"] = vm.Username;
            string chasis = vm.Chasis_number;          
            Session["Chasis_number"] = chasis;
            return Redirect("/Payment/pay?type=R");
        }





    }
}