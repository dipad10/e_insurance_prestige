﻿using E_insurance_prestige.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace E_insurance_prestige.Controllers
{
    public class HelpersController : Controller
    {
        // GET: Helpers
        public JsonResult GetvehicleMakeBrand(string makeid)
        {
            List<string> brands = new Filldropdownhelper().GetVehicleMakebrandById(makeid);

            return Json(brands, JsonRequestBehavior.AllowGet);
        }
    }

}