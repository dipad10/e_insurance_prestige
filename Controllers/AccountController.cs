﻿using E_insurance_prestige.Modules;
using E_insurance_prestige.Viewmodels.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using E_insurance_prestige.Services;
using E_insurance_prestige.Entity;
using System.Configuration;

namespace E_insurance_prestige.Controllers
{
    public class AccountController : Controller
    {
      
        // GET: Account
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginVM vm, string returnurl)
        {
            if(ModelState.IsValid)
            {
                var user = new validationhelper().Validateuser(vm.Username, vm.Password);
                if (user == null)
                {
                    ViewBag.message = "Please enter a correct username and password. Note that both fields may be case-sensitive";
                    ViewBag.messagetype = "error";
                    return View(vm);
                }
                else
                {
                    HelperStoreSqlLog.LogInformation(null, $"Login successful for {vm.Username}");
                    Session["currentuser"] = vm.Username;
                    TempData["success"] = true;
                    TempData["message"] = "Login successful! Welcome back " + vm.Username + " ";
                    if (returnurl != null)
                    {
                        return Redirect(returnurl);
                    }
                    else {
                        //get user permission and redirect to their appropriate dashboards.
                        var permission = new validationhelper().GetUserPermission(vm.Username);
                        if(permission.Equals("USER"))
                        {
                            return Redirect("/Home/Dashboard");
                        }
                        else if(permission.Equals("ADMIN"))
                        {
                            return Redirect("/Admin/Dashboard");
                        }
                    }
                }
            }
          
            return View(vm);
        }

        public ActionResult Logout()
        {
            Session["currentuser"] = null;
            Session.Clear();
            return Redirect("/");
        }

        public ActionResult ForgotPassword()
        {
            ForgotPasswordVM vm = new ForgotPasswordVM();
            return View(vm);
        }
        [HttpPost]
        public ActionResult ForgotPassword(ForgotPasswordVM vm)
        {
            var service = new UsersService();
            var mod = new ModDrive();
            if (ModelState.IsValid)
            {
                var validationhelper = new validationhelper();
                if (validationhelper.CheckEmailAddressExist(vm.EmailAddress))
                {
                    //code to send password to mail and also save activation code
                   var user = service.UpdateUserActivationCode(vm.EmailAddress);
                    //send activation to this email address.
                    string url = $"{ConfigurationManager.AppSettings["pathtoresetpwdtst"]}?M={vm.EmailAddress}&C={user.ActivationCode}";
                    string body = mod.PopulateBodyForgotPassword(user.Insured_name, url);
                    mod.SendHtmlFormattedEmail(user.Email, null, "Reset Your Password", body);
                    ViewBag.message = "A Password Reset link has been sent to your email address.";
                    ViewBag.messagetype = "info";
                    vm.EmailAddress = "";
                    return View(vm);
                }
                else
                {
                    ViewBag.message = "This email address do not match our records!";
                    ViewBag.messagetype = "error";
                    return View(vm);
                }
            }
            //code to check if email address exist
         
            return View(vm);
        }

        public ActionResult ResetPassword()
        {
            ResetPasswordVM vm = new ResetPasswordVM();
            return View(vm);
        }
        [HttpPost]
        public ActionResult ResetPassword(ResetPasswordVM vm)
        {
            if (ModelState.IsValid)
            {
                var validationhelper = new validationhelper();
                var service = new UsersService();
                string useremail = Request.QueryString["M"];
                string activationcode = Request.QueryString["C"];
                if (validationhelper.CheckResetlinkExist(useremail, activationcode))
                {
                    //update the user password
                    var user = service.GetUserByEmail(useremail);
                    user.Password = vm.NewPassword;
                    service.UpdateUser(user);
                    TempData["success"] = true;
                    TempData["message"] = "Password reset successfull!";
                    return RedirectToAction("Login");
                }
            }
                    
            return View(vm);
        }

    }
}