﻿using E_insurance_prestige.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using E_insurance_prestige.ViewModels.SearchFilter;

namespace E_insurance_prestige.Services
{
    public class ScratchcardService
    {
        GibsBrokersDBEntities db = new GibsBrokersDBEntities();

        public List<scratchcard> GetAllScratchCards()
        {
            return db.scratchcards.OrderByDescending(o => o.Date_used).ToList();
        }

        public scratchcard GetScratchCardByPin(string Pin)
        {
            return db.scratchcards.Where(o => o.Epin == Pin).FirstOrDefault();
        }

        public scratchcard GetScratchCardByBatchNo(string Batchno)
        {
            return db.scratchcards.Where(o => o.Batch == Batchno).FirstOrDefault();
        }

        public scratchcard Enable_DisableCard(string type, int? serial)
        {
            var card = db.scratchcards.Where(o => o.serial == serial).FirstOrDefault();
            switch (type)
            {
                case "enable":
                    card.Status = "unused";
                    card.DateUsed = null;
                    card.Used_by = null;
                    break;
                case "disable":
                    card.Status = "blocked";
                    break;
                default:
                    break;
            }
            UpdateScratchcard(card);
            return card;
        }

        public int UpdateScratchcard(scratchcard card)
        {
            return db.SaveChanges();

        }

        public int UpdateScratchcardStatusByPin(string pin, string usedby)
        {
            var card = db.scratchcards.Where(o => o.Epin.Equals(pin)).FirstOrDefault();
            card.Status = "used";
            card.Date_used = DateTime.Now.ToString();
            card.Used_by = usedby;
            return db.SaveChanges();

        }

        public int UpdateScratchcardBranch(string serialnumber, string branch)
        {
            var card = db.scratchcards.Where(o => o.Batch.Equals(serialnumber)).FirstOrDefault();
            card.Branches = branch;         
            return db.SaveChanges();

        }
    }
}