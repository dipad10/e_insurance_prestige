﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using E_insurance_prestige.Entity;
using E_insurance_prestige.Modules;
using E_insurance_prestige.ViewModels.SearchFilter;

namespace E_insurance_prestige.Services
{
    public class UsersService
    {
        GibsBrokersDBEntities db = new GibsBrokersDBEntities();

        public IQueryable<User> GetAllUsers()
        {

            return db.Users.OrderByDescending(p => p.Userid).Take(50);
        }

 
        public User GetUserById(int? id)
        {
            return db.Users.Where(p => p.Userid == id).FirstOrDefault();
        }
        public User GetUserByUsername(string username)
        {
            return db.Users.Where(p => p.Username == username).FirstOrDefault();
        }

        public IQueryable<User> Searchuser(SearchFilterVM filter)
        {
            //if there is filter select all by filter
            var result = db.Users.AsQueryable();

            if (filter != null)
            {
                if (filter.Searchby == "Username")
                {
                    result = result.Where(c => c.Username.Contains(filter.Searchtext));
                }
                else if (filter.Searchby == "Insured_name")
                {
                    result = result.Where(c => c.Insured_name.Contains(filter.Searchtext));
                }
                else if (filter.Date1.HasValue)
                {
                    result = result.Where(x => x.SubmittedOn >= filter.Date1);
                }
                else if (filter.Date2.HasValue)
                    result = result.Where(x => x.SubmittedOn <= filter.Date2);

            }

            return result;
        }

         public int AddUser(User newuser)
        {
            try
            {
                HelperStoreSqlLog.LogInformation(null, $"Adding user {newuser.Insured_name}");
                db.Users.Add(newuser);
                return db.SaveChanges();
            }
            catch (Exception ex)
            {
                HelperStoreSqlLog.LogInformation(null, $"Updating user {newuser.Insured_name}");
                throw ex;
            }
          
        }

        public User GetUserByEmail(string Email)
        {
            return db.Users.Where(o => o.Email.Equals(Email)).FirstOrDefault();

        }
        public User UpdateUserActivationCode(string email)
        {
            try
            {
                var user = GetUserByEmail(email);
                user.ActivationCode = Guid.NewGuid().ToString();
                UpdateUser(user);
                return user;
            }
            catch (Exception ex)
            {
                HelperStoreSqlLog.LogError(ex, "Failed to update User");
                throw ex;
            }
          
        }

        public int UpdateUser(User user)
        {
            try
            {
              
                HelperStoreSqlLog.LogInformation(null, $"Updating user {user.Insured_name}");
                return db.SaveChanges();
            }
            catch (Exception ex)
            {
                HelperStoreSqlLog.LogInformation(ex, $"Updating user {user.Insured_name}");
                throw ex;
            }
         

        }
    }
}