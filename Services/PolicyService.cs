﻿using E_insurance_prestige.Entity;
using E_insurance_prestige.Modules;
using E_insurance_prestige.Viewmodels.Home;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace E_insurance_prestige.Services
{
    public class PolicyService
    {
        GibsBrokersDBEntities db = new GibsBrokersDBEntities();
        ModDrive module = new ModDrive();
        public List<ERegisteration> GetAllPolicies()
        {
            return db.ERegisterations.OrderByDescending(o => o.Certificate_no).ToList();
        }

        public ERegisteration GetPolicyByCertno(int Certno)
        {
            return db.ERegisterations.Where(o => o.Certificate_no == Certno).FirstOrDefault();
        }

        public ERegisteration GetPolicyByCertno2(int? Certno)
        {
            return db.ERegisterations.Where(o => o.Certificate_no == Certno).FirstOrDefault();
        }

        public int UpdatePolicy(ERegisteration policy)
        {
            return db.SaveChanges();

        }

        public ERegisteration AddPolicy(ERegisteration policy)
        {
            db.ERegisterations.Add(policy);
            db.SaveChanges();
            //to return the policy with all details including certno for niid upload
            return policy;

        }

        public ERegisteration MapPolicy(CreatePolicyVM vm, PaymentVM card, string type)
        {
            ModDrive mod = new ModDrive();
            ERegisteration reg = new ERegisteration();
            reg.Insured_name = vm.Insured_name;
            reg.Address = vm.Address;
            reg.Amount = Convert.ToDecimal(vm.Category);
            reg.Card_pin_used = card.Pin;
            reg.Category = vm.categorytext;
            reg.Chasis_number = vm.Chasis_number;
            reg.Colour = vm.Colour;
            reg.Effective_date = DateTime.Now;
            reg.Expiry_date = DateTime.Now.AddDays(-1).AddYears(1);
            reg.Email = vm.Email;
            reg.field01 = getcardbranch(card.Pin);
            reg.Engine_number = vm.Engine_number;
            reg.Identification = vm.Identification;
            reg.Id_number = vm.Id_number;
            reg.Insurance_class = vm.Insurance_class;
            reg.location = vm.location;
            reg.Occupation = vm.Occupation;
            reg.Password = vm.Password;
            reg.Phone = vm.Phone;
            reg.Registeration_number = vm.Registeration_number;
            reg.Title = vm.Title;
            reg.Usage = vm.Usage;
            reg.Username = vm.Username;
            reg.Phone = vm.Phone;
            reg.Vehicle_make = vm.Vehicle_make;
            reg.Vehicle_model = vm.Vehicle_model;
            reg.Vehicle_year = vm.Vehicle_year;
            if(type.Equals("N")) {
                //new policy: generate new policy no
                reg.PolicyNo = mod.GeneratePolicynumber();
                reg.field02 = "NEW";
            }
            else if (type.Equals("R"))
            {
                //renewal: do not generate policy no
                reg.PolicyNo = vm.PolicyNo;
                reg.field02 = "RENEWAL";
            }
            //update autonumber
            mod.UpdateAuto();

            return reg;
        }
        public CreatePolicyVM MapSessionToVM()
        {
            CreatePolicyVM policyvm = new CreatePolicyVM();
            policyvm.Insured_name = HttpContext.Current.Session["Insuredname"].ToString();
            policyvm.Address = HttpContext.Current.Session["Address"].ToString();
            policyvm.Amount = Convert.ToDecimal(HttpContext.Current.Session["Amount"].ToString());
            policyvm.Category = HttpContext.Current.Session["Category"].ToString();
            policyvm.categorytext = HttpContext.Current.Session["Category"].ToString();
            policyvm.Chasis_number = HttpContext.Current.Session["Chasis_number"].ToString();
            policyvm.Colour = HttpContext.Current.Session["Colour"].ToString();
            policyvm.Email = HttpContext.Current.Session["Email"].ToString();
            policyvm.Engine_number = HttpContext.Current.Session["Engine_number"].ToString();
            policyvm.Identification = HttpContext.Current.Session["Identification"].ToString();
            policyvm.Id_number = HttpContext.Current.Session["Identification_no"].ToString();
            policyvm.Insurance_class = HttpContext.Current.Session["Insurance_class"].ToString();
            policyvm.location = HttpContext.Current.Session["location"].ToString();
            policyvm.Occupation = HttpContext.Current.Session["Occupation"].ToString();
            policyvm.Password = HttpContext.Current.Session["Password"].ToString();
            policyvm.Phone = HttpContext.Current.Session["Phone"].ToString();
            policyvm.Registeration_number = HttpContext.Current.Session["Registration_number"].ToString();
            policyvm.Title = HttpContext.Current.Session["Title"].ToString();
            policyvm.Usage = HttpContext.Current.Session["Usage"].ToString();
            policyvm.Username = HttpContext.Current.Session["Username"].ToString();
            policyvm.Vehicle_make = HttpContext.Current.Session["Vehicle_make"].ToString();
            policyvm.Vehicle_model = HttpContext.Current.Session["Vehicle_model"].ToString();
            policyvm.Vehicle_year = HttpContext.Current.Session["Vehicle_year"].ToString();

            return policyvm;
        }

        //to update policy branch with the scarcth card branch used
        public string updatebranch(int certno, string ScratchcardPin)
        {
            var card = new ScratchcardService().GetScratchCardByPin(ScratchcardPin);
            var policy = new PolicyService().GetPolicyByCertno(certno);
            policy.field01 = card.Branches;
            UpdatePolicy(policy);

            return card.Branches;
        }

        public string getcardbranch(string pin)
        {
            var card = new ScratchcardService().GetScratchCardByPin(pin);
            return card.Branches;
        }


        public List<ERegisteration> GetPoliciesByUser(string username)
        {
            return db.ERegisterations.Where(o => o.Username.Equals(username)).ToList();
        }
        public ERegisteration GetPolicybyRegNo(string regno)
        {
            return db.ERegisterations.OrderByDescending(o => o.Registeration_number.Equals(regno)).FirstOrDefault();
        }

        public int GetUserTotalPolicyCount(string username)
        {
            return db.ERegisterations.Where(o => o.Username.Equals(username)).Count();
        }

        public decimal? GetUserTotalSumInsured(string username)
        {
            return db.ERegisterations.Where(o => o.Username.Equals(username)).Select(o => o.Amount).Sum();
        }

        public int GetUserTotalDueForRenewal(string username)
        {
            //need to fix cases where customer already done renewal.
            return db.ERegisterations.Where(o => o.Username.Equals(username) && o.field02 == "EXPIRED").Count();
        }

        public int GetTotalPolicyCount()
        {
            return db.ERegisterations.Count();
        }

        public string GetTotalSumInsured()
        {
            var totalsum = db.ERegisterations.Select(o => o.Amount).Sum();
            return module.FormatNumber(Convert.ToInt32(totalsum));
        }
        public int GetTotalDueForRenewal()
        {
            //need to fix cases where customer already done renewal.
            return db.ERegisterations.Where(o => o.field02 == "EXPIRED").Count();
        }
    }
}