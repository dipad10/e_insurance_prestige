﻿using E_insurance_prestige.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace E_insurance_prestige.Services
{
    public class BranchesService
    {
        GibsBrokersDBEntities db = new GibsBrokersDBEntities();

        public List<Branch> GetAllBranches()
        {
            return db.Branches.OrderByDescending(o => o.SubmittedOn).ToList();
        }

        public Branch GetBranchBySN(int SN)
        {
            return db.Branches.Where(o => o.SN == SN).FirstOrDefault();
        }

      

        public int UpdateBranch(Branch branch)
        {
            return db.SaveChanges();

        }
        public int DeleteBranch(int? id)
        {
            var branch = db.Branches.Where(o => o.SN == id).FirstOrDefault();
            db.Branches.Remove(branch);
            return db.SaveChanges();

        }


    }
}
