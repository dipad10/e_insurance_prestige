﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace E_insurance_prestige.Viewmodels.Home
{
    public class CreatePolicyVM
    {
        public int Certificate_no { get; set; }
        [Required]
        public string Username { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Insured_name { get; set; }
        [Required]
        public string location { get; set; }
        [Required]
        public string Identification { get; set; }
        [Required]
        public string Id_number { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string Phone { get; set; }
        [Required]
        public string Occupation { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public string Insurance_class { get; set; }
        [Required]
        public string Vehicle_make { get; set; }
        [Required]
        public string Vehicle_model { get; set; }
        [Required]
        public string Vehicle_year { get; set; }
        [Required]
        public string Registeration_number { get; set; }
        [Required]
        public string Engine_number { get; set; }
        [Required]
        public string Chasis_number { get; set; }
        [Required]
        public string Colour { get; set; }
        [Required]
        public string Category { get; set; }
        [Required]
        public string Usage { get; set; }
        [Required]
        public decimal Amount { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [DataType(DataType.Password)]
        [Compare("Password")]
        [Required]
        public string ConfirmPassword { get; set; }
        public string categorytext { get; set; }
        public string PolicyNo { get; set; }


    }
}

