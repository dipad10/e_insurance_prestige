﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace E_insurance_prestige.Viewmodels.Account
{
    public class User_VM
    {
        public int Userid { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Title { get; set; }
        public string Insured_name { get; set; }
        public string location { get; set; }
        public string Identification { get; set; }
        public string Id_number { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Occupation { get; set; }
        public string Address { get; set; }
        public string field01 { get; set; }
        public string field02 { get; set; }
        public string field03 { get; set; }
        public string field04 { get; set; }
        public string field05 { get; set; }
        public DateTime? SubmittedOn { get; set; }
        public string Permissions { get; set; }
    }
}