﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace E_insurance_prestige.Viewmodels.Account
{
    public class ForgotPasswordVM
    {
        [Required]
        public string  EmailAddress { get; set; }
    }
}