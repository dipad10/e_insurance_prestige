﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace E_insurance_prestige.Viewmodels.Home
{
    public class PaymentVM
    {
        public int ID { get; set; }
        public string Pin { get; set; }
    }
}