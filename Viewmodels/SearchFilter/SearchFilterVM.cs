﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace E_insurance_prestige.ViewModels.SearchFilter
{
    public class SearchFilterVM
    {
        public string Searchby { get; set; }
        public string Searchtext { get; set; }
        public DateTime? Date1 { get; set; }
        
        public DateTime? Date2 { get; set; }
        public string Groupname { get; set; }
    }
}