﻿using E_insurance_prestige.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace E_insurance_prestige.Viewmodels.Admin
{
    public class Allocate_cardsVM
    {
        public List<scratchcard> cards { get; set; }
        [Required]
        public string SerialNumbers { get; set; }
        [Required]
        public string Branch { get; set; }
    }
}