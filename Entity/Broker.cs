//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace E_insurance_prestige.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class Broker
    {
        public string BrokerID { get; set; }
        public string InsCompanyID { get; set; }
        public string BrokerName { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string MobilePhone { get; set; }
        public string ContactPerson { get; set; }
        public string Password { get; set; }
        public System.DateTime SubmitDate { get; set; }
        public string Rate { get; set; }
        public string Value { get; set; }
    }
}
