//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace E_insurance_prestige.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class State
    {
        public int RegionID { get; set; }
        public string Region { get; set; }
        public Nullable<int> ZoneID { get; set; }
        public string region_code { get; set; }
        public byte[] Lastupdate { get; set; }
    }
}
