﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace E_insurance_prestige.Extensions
{
    public static class MakeActive
    {
        public static string MakeActiveaction(this UrlHelper urlHelper, string action)
        {
            string result = "active";

            string actionName = urlHelper.RequestContext.RouteData.Values["action"].ToString();

            if (!actionName.Equals(action, StringComparison.OrdinalIgnoreCase))
            {
                result = null;
            }

            return result;
        }

  

    }
}